# Karim Boudjema


After a long time, I decided to create this repo to give something back to the Drupal Community. 

This repo we be almost used as a code repository for my Drupal blog: http://www.karimboudjema.com

# Who I am?
Hello

I'm Karim Boudjema or KarimB in most places online. I’m a Belgian web developer and business administrator living in Cochabamba - Bolivia.

When I discovered Drupal 4.7 back in 2008 (yes… 10 years ago), I quickly felt that it would be a major move in the digital industry. And it was! But Drupal also changed my live and my day to day job.

Working mainly for the media industry, I’ve developed large Drupal websites like newspapers sites or marketplaces with hundreds of thousands of nodes and millions of visitors each month. Additionally, my business manager background drove me to help many clients develop a strong and effective digital transformation plan.

# What do I do with Drupal?
+Newspapers web sites

+Apache Solr for facet search and views backend

+Playing with Memcache and Varnish

+Multilingual sites

+Drupal Commerce

# What do I love?
+My wife and my two kids who support me in this journey

+The Drupal Community

+Traveling a lot

+Listening jazz, heavy metal and french pop songs (What a mix!!!)

+Hiking in the surrounding andean mountains

+Developing NGO’s Drupal website for free



Please read [my blog](http://karimboudjema.com/) or [get in touch](http://karimboudjema.com/en/contact).





